#!/bin/bash
#-------------------------------------------------------------------------------
# French ANR ASAWoO Project
#
# Deployment of OSGi platforms
#
# author: Lionel Touseau
# CASA research group, IRISA Laboratory, Université de Bretagne Sud
# version : 0.1.0
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# usage
#-------------------------------------------------------------------------------
usage(){
cat << EOF
Usage:
   ./bin/copy_bundles <root_bundle_dir>

   - root_bundle_dir: Typically the directory where osgi bundles are stored
EOF
}

#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------

if [[ $# < 1 ]]; then
   	usage
else
	# ASAWoO version
	asawoo_version=0.2.0-SNAPSHOT

	if [[ "$ASAWOO_ROOT_DIR" == "" ]]; then
		defaultDir="$(dirname "$(pwd)")"
		export ASAWOO_ROOT_DIR=$defaultDir
		echo "Environment variable ASAWOO_ROOT_DIR is not set. Using $defaultDir as default."
	fi
	if [[ "$ASAWOO_BUNDLES" == "" ]]; then
		export ASAWOO_BUNDLES=$ASAWOO_ROOT_DIR/platform/dependencies
	fi

	# Create directory structure
	if [ ! -d $1/lib ]; then
		echo "Creating directory structure"
		# add missing directories
		mkdir -p $1/lib/vertx
		mkdir -p $1/avatar
		mkdir -p $1/app/domains
		mkdir -p $1/app/raspi
		mkdir -p $1/app/demo
		mkdir -p $1/app/wotapps
	fi

	echo "Copy librairies"
	cp -r $ASAWOO_BUNDLES/lib/* $1/lib/
	cp $ASAWOO_BUNDLES/third-party/fr.c3po.framework.osgi.services-1.0.0.jar $1/lib/
	cp $ASAWOO_BUNDLES/third-party/fr.c3po.framework.osgi.implementation-1.0.0.jar $1/lib/
#	cp osgi-bundles/ibrdtn-api-osgi-1.0.5-SNAPSHOT.jar $1/ibrdtn-api-osgi-1.0.5-SNAPSHOT.jar
	echo "Copy Vertx"
	cp $ASAWOO_BUNDLES/third-party/vertx/asawoo.vertx-osgi-3.4.0.jar $1/lib/vertx/

	echo "Copy ASAWoO core and managers"
	find $ASAWOO_ROOT_DIR/avatar/ -wholename "*/target/*-$asawoo_version.jar" -exec cp {} $1/avatar \;

	echo "Copy ASAWoO Domains"
	find $ASAWOO_ROOT_DIR/applications/domains/ -wholename "*/target/*-$asawoo_version.jar" -exec cp {} $1/app/domains \;

	echo "Copy application-specific bundles"
	find $ASAWOO_ROOT_DIR/applications/things -wholename "*/target/*-$asawoo_version.jar" ! -path "*/raspi/*" -exec cp {} $1/app \;
	
	echo "Copy demo bundles"
	find $ASAWOO_ROOT_DIR/applications/demos -wholename "*/target/*-$asawoo_version.jar" -exec cp {} $1/app/demo \;

	echo "Copy wotapps bundles"
	find $ASAWOO_ROOT_DIR/applications/wotapps -wholename "*/target/*-$asawoo_version.jar" -exec cp {} $1/app/wotapps \;

	echo "Copy RaspberryPi bundles"
	find $ASAWOO_ROOT_DIR/applications/things/raspi -wholename "*/target/*-$asawoo_version.jar" -exec cp {} $1/app/raspi \;


fi
