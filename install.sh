echo ""
echo "ASAWoO - Populate the code repository using the official project's source code and serve it over HTTP."
echo ""

mkdir ~/.asawoo
curl -s https://gitlab.com/asawoo/code-repo/raw/master/docker-compose.yml -o ~/.asawoo/docker-compose.code-repo.yml || exit 1
docker network create asawoo
docker-compose -f ~/.asawoo/docker-compose.code-repo.yml pull || exit 1
docker stop code-repo-deploy && docker rm code-repo-deploy
docker stop code-repo-http && docker rm code-repo-http
docker-compose -f ~/.asawoo/docker-compose.code-repo.yml up -d || exit 1

echo ""
echo "ASAWoO - The code repo is up :"
echo "  - check the jars building logs : docker logs -f code-repo-deploy"
echo "  - view the XML file : http://localhost:3131/index.xml"
echo ""
