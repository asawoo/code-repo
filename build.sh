#!/bin/ash

echo "Retrieving or updating source repositories"
cd /sources
if cd avatar; then git pull; else git clone https://gitlab.com/asawoo/avatar.git; fi
cd /sources
if cd applications; then git pull; else git clone https://gitlab.com/asawoo/applications.git; fi

echo "Installing dependencies"
cd /root/dependencies
mvn -q org.apache.maven.plugins:maven-install-plugin:2.5.1:install-file -Dpackaging=jar -Dfile=asawoo.vertx-osgi-3.4.0.jar
mvn -q org.apache.maven.plugins:maven-install-plugin:2.5.1:install-file -Dpackaging=jar -Dfile=fr.c3po.framework.osgi.services-1.0.0.jar
mvn -q org.apache.maven.plugins:maven-install-plugin:2.5.1:install-file -Dpackaging=jar -Dfile=ibrdtn-api-osgi-1.0.5-SNAPSHOT.jar

echo "Building jars from avatar"
cd /sources/avatar && mvn -q clean install
echo "Building jars from applications"
cd /sources/applications && mvn -q clean install
echo "Building index.xml"
cd /root
eval `mvn -q install 2>&1 | grep -v "WARNING: Could"`
