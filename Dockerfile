FROM maven:3.5.0-jdk-8

WORKDIR /root
ADD build.sh build.sh
ADD pom.xml pom.xml
ADD dependencies dependencies

VOLUME /sources
VOLUME /root/.m2/repository

CMD ["/bin/bash", "build.sh"]
